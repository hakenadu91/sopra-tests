package de.openjdk11.gradle;

import javafx.animation.ScaleTransition;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.util.Duration;

/**
 * Demo f&uuml;r den Einsatz von Gradle + OpenJDK 11.0.2 + OpenJFX 11.0.2
 * 
 * @author Manuel
 * @since 23.01.2019
 */
public final class MainApp extends Application {

	@Override
	public void start(final Stage stage) throws Exception {
		stage.setTitle("Test-Gradle");

		final VBox root = new VBox();
		root.getStyleClass().add("root");

		final Button contentButton = new Button("Fancy Button");
		contentButton.prefWidthProperty().bind(root.widthProperty().divide(2));
		/*
		 * Method References Test
		 */
		contentButton.setOnAction(this::onContentButtonClick);
		contentButton.disableProperty().bind(contentButton.scaleXProperty().greaterThan(1D));

		final Label titleLabel = new Label("OpenJFX 11");
		titleLabel.getStyleClass().add("title-label");
		titleLabel.rotateProperty().bind(contentButton.scaleXProperty().multiply(360D));

		root.getChildren().addAll(titleLabel, contentButton);

		final Scene scene = new Scene(root, 500D, 500D);
		scene.getStylesheets().add(getClass().getResource("/stylesheets/styles.css").toExternalForm());

		stage.setScene(scene);
		stage.show();
	}

	private void onContentButtonClick(final ActionEvent event) {
		/*
		 * Casten = Boese, aber in diesem Fall ists ja nur ne Demo und wir haben k. B.
		 * auf Class Member
		 */
		final Button source = (Button) event.getSource();
		final ScaleTransition scaleTransition = new ScaleTransition(Duration.millis(500D), source);
		scaleTransition.setToX(getToX());
		scaleTransition.setToY(1.1D);
		scaleTransition.setAutoReverse(true);
		scaleTransition.setCycleCount(2);
		scaleTransition.play();
	}

	/**
	 * Diese Methode testen wir mittels JUnit 5 :)
	 * 
	 * @return stumpfer konstanter Wert f&uuml;r
	 *         {@link ScaleTransition#setToX(double)}
	 */
	public final double getToX() {
		return 1.5D;
	}

	public static void main(final String[] args) {
		launch(args);
	}
}