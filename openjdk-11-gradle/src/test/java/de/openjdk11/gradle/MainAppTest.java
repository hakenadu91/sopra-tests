package de.openjdk11.gradle;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import de.openjdk11.gradle.MainApp;

/**
 * Ein Beispiel f&uuml;r einen JUnit 5 Test
 * 
 * @author Manuel
 * @since 23.01.2019
 */
public final class MainAppTest {

	private static MainApp mainApp;

	@BeforeAll
	public static void setUpBeforeAll() {
		mainApp = new MainApp();
	}

	@Test
	public void testGetToX() {
		final double toX = mainApp.getToX();
		assertEquals(1.5D, toX, "MainApp#getToX lieferte fehlerhaften Wert");
	}
}
